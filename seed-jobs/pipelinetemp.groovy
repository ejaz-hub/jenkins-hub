pipelineJob('pipeline-env2') {
  description('')
  displayName('pipeline-env2')
  keepDependencies(false)
  logRotator {
    artifactDaysToKeep(-1)
    artifactNumToKeep(-1)
    daysToKeep(5)
    numToKeep(25)
  }
  properties {
  }
  
  configure { flowdefinition ->
    flowdefinition / 'actions' << 'org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobAction'(plugin:'pipeline-model-definition')

    flowdefinition / 'actions' << 'org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction'(plugin:'pipeline-model-definition') {

      'jobProperties'()
      'triggers'()
      'parameters'()
      'options'()
    }

    flowdefinition << delegate.'definition'(class:'org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition',plugin:'workflow-cps') {

      'scm'(class:'hudson.plugins.git.GitSCM',plugin:'git') {
        'configVersion'(2)
        'userRemoteConfigs' {
          'hudson.plugins.git.UserRemoteConfig' {
            'url'('https://gitlab.com/atulyw/devops-b4-jenkins.git')
            'credentialsId'('gitlab')
          }
        }
        'branches' {
          'hudson.plugins.git.BranchSpec' {
            'name'('*/master')
          }
        }
        'doGenerateSubmoduleConfigurations'(false)
        'submoduleCfg'(class:'list')
        'extensions'()
      }
      'scriptPath'('default-env.jdp')
      'lightweight'(false)
    }
  }
}
