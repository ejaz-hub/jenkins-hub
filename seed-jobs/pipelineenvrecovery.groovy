pipelineJob('pipeline-env_converted') {
  description('')
  displayName('pipeline-env_converted')
  keepDependencies(false)
  logRotator {
    artifactDaysToKeep(-1)
    artifactNumToKeep(-1)
    daysToKeep(10)
    numToKeep(20)
  }
  properties {
  }
  quietPeriod(0)
  checkoutRetryCount(0)
  disabled(false)
  concurrentBuild(false)
  configure { flowdefinition ->
    flowdefinition / 'actions' << 'org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobAction'(plugin:'pipeline-model-definition')

    flowdefinition / 'actions' << 'org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction'(plugin:'pipeline-model-definition') {

      'jobProperties'()
      'triggers'()
      'parameters'()
      'options'()
    }

    flowdefinition << delegate.'definition'(class:'org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition',plugin:'workflow-cps') {

      'scm'(class:'hudson.plugins.git.GitSCM',plugin:'git') {
        'configVersion'(2)
        'userRemoteConfigs' {
          'hudson.plugins.git.UserRemoteConfig' {
            'url'('https://gitlab.com/atulyw/devops-b4-jenkins.git')
            'credentialsId'('gitlab')
          }
        }
        'branches' {
          'hudson.plugins.git.BranchSpec' {
            'name'('*/master')
          }
        }
        'doGenerateSubmoduleConfigurations'(false)
        'submoduleCfg'(class:'list')
        'extensions'()
      }
      'scriptPath'('default-env.jdp')
      'lightweight'(false)
    }
  }
}
