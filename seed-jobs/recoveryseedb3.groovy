pipelineJob('seed-b5') {

  description('Job for API Pre-Prod ENV')

  displayName('seed-b5')

  keepDependencies(false)

  logRotator {

    artifactDaysToKeep(-1)

    artifactNumToKeep(-1)

    daysToKeep(5)

    numToKeep(10)

  }

  properties {

  }
  configure { flowdefinition ->

    flowdefinition / 'properties' << 'org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty' {

      'triggers' {

        'hudson.triggers.TimerTrigger' {

          'spec'('H */2 * * *')

        }

      }

    }

    flowdefinition << delegate.'definition'(class:'org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition',plugin:'workflow-cps') {

      'scm'(class:'hudson.plugins.git.GitSCM',plugin:'git') {

        'configVersion'(2)

        'userRemoteConfigs' {

          'hudson.plugins.git.UserRemoteConfig' {

            'url'('https://gitlab.com/atulyw/devops-b4-jenkins.git')

            'credentialsId'('gitlab')

          }

        }

        'branches' {

          'hudson.plugins.git.BranchSpec' {

            'name'('*/test')

          }

        }

        'doGenerateSubmoduleConfigurations'(false)

        'submoduleCfg'(class:'list')

        'extensions'()

      }

      'scriptPath'('default-env.jdp')

      'lightweight'(true)

    }

  }

}
